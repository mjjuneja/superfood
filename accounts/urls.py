from rest_framework_jwt.views import refresh_jwt_token,verify_jwt_token
from django.urls import include, path
from . import views

urlpatterns = [
    path('auth/register/', views.Register),
    path('auth/login/', views.Login),
    path('auth/logout/', views.Logout),
    path('auth/refresh',refresh_jwt_token)
]
