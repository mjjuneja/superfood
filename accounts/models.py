from django.contrib.auth.models import AbstractUser,BaseUserManager,AbstractBaseUser,PermissionsMixin
from django.db import models



class CustomUserManager(BaseUserManager):
    # def create_user(self, email, user_role, password):
    #     # if not email:
    #     #     raise ValueError('Users must have an email address')
    #     if email:
    #         user = self.model(
    #             email=self.normalize_email(email),
    #             username=self.normalize_email(email),

    #         )
    #     user.set_password(password)
    #     user.save(using=self._db)
    #     return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.model(
                username=username,
            )
        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user



USER_ROLES = [
    (1,'Customer'),
    (2,'Owner'),
    (3,'Admin'),
]


class CustomUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        null=True,
    )

    phone = models.BigIntegerField(
        verbose_name='phone number',
        unique=True,
        null=True,
    )

    username = models.CharField(max_length=255,null=True,unique=True)
    
    user_role = models.CharField(max_length=50,default=1,choices=USER_ROLES)
    password_reset_token = models.CharField(max_length=255,blank=True,null=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)    


    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        val = ''
        if self.full_name:
            val =  self.full_name + "( "
        if self.email:
            val+= self.email
        elif self.phone:
            val+= str(self.phone)
        else:
            val+= str(self.id)
        return val + " )"

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def has_role(self,role):
        if self.user_role == role:
            return True
        else:
            return False

    @property
    def is_staff(self):
        return self.is_admin