from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model,login,authenticate,logout
from django.contrib.auth.models import Group
from rest_framework_jwt.settings import api_settings
from django.contrib.auth.decorators import login_required

from .serializers import userSerializer,loginSerializer,usrSerializer

User = get_user_model()

def giveCustomerPermissions(one_user):
    gg = Group.objects.get(name='Customer')
    gg.user_set.add(one_user)


def giveOwnerPermissions(one_user):
    gg = Group.objects.get(name='Owner')
    gg.user_set.add(one_user)

"""
View to register a new User:
    POST ->
    @param HttpRequest

    @return JsonObject

    
"""
@api_view(['GET', 'POST'])
@csrf_exempt
def Register(request):
    if request.method == 'POST':
        ser = userSerializer(data=request.data)
        if ser.is_valid():
            print('----------------------------------')
            serData = ser.data
            newUser = User.objects.create(
                username = serData['username'],
                email = serData['email'],
            )
            if serData['role'] == 'Owner':
                giveOwnerPermissions(newUser)
                newUser.user_role = 2
            elif serData['role'] == 'Customer':
                giveCustomerPermissions(newUser)
                newUser.user_role = 1
            
            newUser.set_password(serData['password'])
            newUser.save()
            login(request,newUser,backend='django.contrib.auth.backends.ModelBackend')
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

            payload = jwt_payload_handler(newUser)
            token = jwt_encode_handler(payload)
            usr_data = usrSerializer(newUser)
            return Response({
                'token':token,
                'user':usr_data.data
            })


"""
View to login a User:
    POST ->
    @param HttpRequest

    @return JsonObject

    
"""
@api_view(['GET', 'POST'])
@csrf_exempt
def Login(request):
    if request.method == 'POST':
        ser = loginSerializer(data=request.data)
        if ser.is_valid():
            autheduser = authenticate(username=ser.data['username'],password=ser.data['password'])
            if autheduser is not None:
                login(request,autheduser,backend='django.contrib.auth.backends.ModelBackend')
                jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

                payload = jwt_payload_handler(autheduser)
                token = jwt_encode_handler(payload)
                usr_data = usrSerializer(autheduser)
                return Response({
                    'token':token,
                    'user':usr_data.data
                })


"""
View to logout a User:
    GET ->
    @param HttpRequest

    @return JsonObject

    
"""

@api_view(['GET', 'POST'])
@csrf_exempt
@login_required
def Logout(request):
    if request.method == 'GET':
        logout(request)
        return Response({
            'msg':'user logged out'
        })