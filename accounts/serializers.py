from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()

class userSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50)
    email = serializers.CharField(max_length=50,required=False)
    role = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=50)


class usrSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','email','groups')


class loginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=50)