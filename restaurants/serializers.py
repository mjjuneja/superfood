from rest_framework import serializers
from . import models

class replySerializer(serializers.ModelSerializer):
    owner_rel = serializers.SerializerMethodField()
    class Meta:
        model = models.Reply
        fields = "__all__"
    
    def get_owner_rel(self,obj):
        return obj.owner_rel.username



class reviewSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    rest = serializers.SerializerMethodField()
    reply = replySerializer()
    class Meta:
        model = models.Review
        fields = ('id','rating','content','reply','username','rest')

    def get_username(self,obj):
        return obj.customer_rel.username

    def get_rest(self,obj):
        return obj.restaurant_rel.name

class addReviewSerializer(serializers.Serializer):
    rating = serializers.IntegerField()
    content = serializers.CharField(max_length=255)
    restaurant_rel =serializers.IntegerField()


class addrestaurantSerializer(serializers.ModelSerializer):
    reviews = serializers.SerializerMethodField()
    class Meta:
        model = models.Restaurant
        fields = "__all__"

class restaurantSerializer(serializers.ModelSerializer):
    reviews = serializers.SerializerMethodField()
    class Meta:
        model = models.Restaurant
        fields = "__all__"

    def get_reviews(self,obj):
        results = []
        results.append(models.Review.objects.filter(restaurant_rel=obj).order_by('-rating').first())
        results.append(models.Review.objects.filter(restaurant_rel=obj).order_by('-rating').last())
        if results[0] is not None:
            allrs = list(models.Review.objects.filter(restaurant_rel=obj).order_by('rating'))
            del allrs[0]
            del allrs[-1]
            results.extend(allrs)
            revs = reviewSerializer(results,many=True)
            print (results)
            return revs.data
        else :
            return None


class addreplySerializer(serializers.Serializer):
    content = serializers.CharField(max_length=255)
    review_id = serializers.IntegerField()