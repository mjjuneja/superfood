from django.urls import include, path
from . import views

restaurant_list = views.RestaurantList.as_view({
    'get': 'list',
    'post': 'create'
})
restaurant_detail = views.RestaurantList.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    path('restaurants/', restaurant_list, name='restaurant-list'),
    path('restaurants/<int:pk>/', restaurant_detail, name='restaurant-detail'),
    path('myrests/',views.myRestaurant.as_view()),
    # path('addrest/',views.addRestaurant.as_view()),
    path('getownrevs/',views.getOwnerReviews),
    path('getrestrevs/',views.getRestReviews.as_view()),
    path('reviews/', views.ReviewList.as_view()),
    path('addreview/', views.addRating),
    path('reply/', views.ReplyList.as_view()),
    path('addreply/', views.addReply),
]
