from django.contrib import admin

# Register your models here.
from django.apps import apps

app = apps.get_app_config('restaurants')

for model_name,model in app.models.items():
    print(model_name)
    admin.site.register(model)
