from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator, MaxValueValidator

User = get_user_model()

class Restaurant(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)
    img = models.CharField(max_length=255,null=True)
    user_rel = models.ForeignKey(User,on_delete=models.CASCADE)
    description = models.TextField(editable=True)
    avg_rating = models.IntegerField(default=0,validators=[MaxValueValidator(5),MinValueValidator(0)])
    
class Reply(models.Model):
    id = models.AutoField(primary_key=True)
    # review_rel = models.ForeignKey(Review,on_delete=models.CASCADE)
    content = models.TextField(editable=True)
    owner_rel = models.ForeignKey(User,on_delete=models.CASCADE)

class Review(models.Model):
    id = models.AutoField(primary_key=True)
    restaurant_rel = models.ForeignKey(Restaurant,on_delete=models.CASCADE)
    rating = models.IntegerField(default=0,validators=[MaxValueValidator(5),MinValueValidator(0)])
    content = models.TextField(editable=True)
    reply = models.OneToOneField(Reply,null=True,on_delete=models.SET_NULL)
    customer_rel = models.ForeignKey(User,on_delete=models.CASCADE)


