from django.shortcuts import render

# Create your views here.
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from rest_framework.permissions import IsAuthenticated
from . import models
from . import serializers
from django.views.generic.detail import DetailView
from rest_framework import viewsets
from rest_framework.generics import ListAPIView,CreateAPIView
from django.shortcuts import get_object_or_404,get_list_or_404

# View to list and create Restaurants:
#     POST GET ->
#     @param HttpRequest

#     @return JsonObject

class RestaurantList(viewsets.ModelViewSet):
    queryset = models.Restaurant.objects.all()
    serializer_class = serializers.restaurantSerializer
    # permission_classes = (IsAuthenticated,)
    paginate_by = 100


# class addRestaurant(CreateAPIView):
#     queryset = models.Restaurant.objects.all()
#     serializer_class = serializers.addrestaurantSerializer
#     permission_classes = (IsAuthenticated,)

class myRestaurant(ListAPIView):
    def get_queryset(self):
        return models.Restaurant.objects.filter(user_rel=self.request.user)
    serializer_class = serializers.restaurantSerializer
    permission_classes = (IsAuthenticated,)


class getRestReviews(ListAPIView):
    def get_queryset(self) :
        return models.Review.objects.filter(restaurant_rel=get_object_or_404(models.Restaurant,id=self.request.data['rest_id']))
    serializer_class = serializers.reviewSerializer
    permission_classes = (IsAuthenticated,)

@api_view(['GET', 'POST'])
@csrf_exempt
@login_required
def getOwnerReviews(request):
    if request.method=='GET':
        allRests = get_list_or_404(models.Restaurant,user_rel=request.user)
        print('-----------')
        results = []
        for r in allRests:
            try:
                revs = models.Review.objects.filter(restaurant_rel=r).filter(reply=None)
            except:
                revs = None
            results.extend(revs)
        print('============')
        rser = serializers.reviewSerializer(results,many=True)
        return Response(rser.data)


class RestaurantDetail(DetailView):
    model = models.Restaurant
    serializer_class = serializers.restaurantSerializer
    permission_classes = (IsAuthenticated,)
    paginate_by = 100


# View to list and create Review:
#     POST GET->
#     @param HttpRequest

#     @return JsonObject


class ReviewList(generics.ListCreateAPIView):
    queryset = models.Review.objects.all()
    serializer_class = serializers.reviewSerializer
    permission_classes = (IsAuthenticated,)
    paginate_by = 100



# View to list and create Reply to a Review:
#     POST GET->
#     @param HttpRequest

#     @return JsonObject

class ReplyList(generics.ListCreateAPIView):
    queryset = models.Reply.objects.all()
    serializer_class = serializers.replySerializer
    permission_classes = (IsAuthenticated,)
    paginate_by = 100

from .serializers import addReviewSerializer
from django.shortcuts import get_object_or_404

@api_view(['GET', 'POST'])
@csrf_exempt
@login_required
def addRating(request):
    if request.method == 'POST':
        ser = addReviewSerializer(data=request.data)
        if ser.is_valid():
            newReview = models.Review.objects.create(
                restaurant_rel = get_object_or_404(models.Restaurant,id=ser.data['restaurant_rel']),
                rating = ser.data['rating'],
                content = ser.data['content'],
                customer_rel = request.user                                   
            )
            newReview.save()
            allReviews = models.Review.objects.filter(restaurant_rel = newReview.restaurant_rel)
            totalRev = models.Review.objects.filter(restaurant_rel = newReview.restaurant_rel).count()
            sumr = 0
            for review in allReviews:
                sumr += review.rating
            rel_rest = get_object_or_404(models.Restaurant,id=ser.data['restaurant_rel'])
            rel_rest.avg_rating = sumr/totalRev
            print('-------------------->',rel_rest.avg_rating)
            rel_rest.save()
            return Response({
                'detail': 'Review added'
            })



@api_view(['GET', 'POST'])
@csrf_exempt
@login_required
def addReply(request):
    if request.method == 'POST':
        rser = serializers.addreplySerializer(data=request.data)
        if rser.is_valid():
            newrep = models.Reply.objects.create(
                owner_rel=request.user,
                content = rser.data['content']
            )
            newrep.save()
            rev = get_object_or_404(models.Review,id=rser.data['review_id'])
            rev.reply = newrep
            rev.save()
            return Response('Replied')
